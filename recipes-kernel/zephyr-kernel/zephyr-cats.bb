require recipes-kernel/zephyr-kernel/zephyr-sample.inc

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://cats/LICENSE;md5=86d3f3a95c324c9479bd8986968f4327"

SRC_URI += " \
    git://gitlab.eclipse.org/eclipse/oniro-blueprints/context-aware-touch-screen/context-aware-touch-screen.git;protocol=https;branch=main;name=cats;destsuffix=git/cats \
"
SRCREV = "e3566df877be205af24e2473aa22946effe1a053"

ZEPHYR_SRC_DIR = "${S}/cats"

EXTRA_OECMAKE:append =" -DSHIELD=adafruit_2_8_tft_touch_v2"

COMPATIBLE_MACHINE = "(arduino-nano-33-ble)"

PACKAGECONFIG ?= "demo keypad"
PACKAGECONFIG[demo] = "-DCONFIG_CATS_DEMO_SCREEN=y,-DCONFIG_CATS_DEMO_SCREEN=n"
PACKAGECONFIG[keypad] = "-DCONFIG_CATS_KEYPAD_SCREEN=y,-DCONFIG_CATS_KEYPAD_SCREEN=n"
PACKAGECONFIG[developer] = "-DCONFIG_CATS_DEVELOPER_SCREEN=y,-DCONFIG_CATS_DEVELOPER_SCREEN=n"
